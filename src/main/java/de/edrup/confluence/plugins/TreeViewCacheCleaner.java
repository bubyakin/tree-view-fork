package de.edrup.confluence.plugins;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.quartz.jobs.AbstractJob;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class TreeViewCacheCleaner extends AbstractJob {

	private final TreeViewHelper treeViewHelper;
	private final TransactionTemplate transactionTemplate;
	private static final Logger log = LoggerFactory.getLogger(TreeViewCleaner.class);

	
	public TreeViewCacheCleaner(TreeViewHelper treeViewHelper, TransactionTemplate transactionTemplate) {
		this.treeViewHelper = treeViewHelper;
		this.transactionTemplate = transactionTemplate;
	}

	
	// things to do in the task
	@Override
	public void doExecute(JobExecutionContext jobExecutionContext)
			throws JobExecutionException {
		// as we can't be sure that the connection to teh DB is open we have to do this in a transaction
		transactionTemplate.execute(new TransactionCallback<Object>()
		{
		    @Override
		    public Object doInTransaction()
		    {
				log.debug("Cache cleaner task was started!");
				treeViewHelper.clearPageEventCache();
				return null;
			}
		});
	}
}