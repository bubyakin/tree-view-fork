package de.edrup.confluence.plugins;

import org.springframework.beans.factory.DisposableBean;

import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageMoveEvent;
import com.atlassian.confluence.event.events.content.page.PageViewEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

public class TreeViewListener implements DisposableBean{

	private final TreeViewHelper treeViewHelper;
	private final EventPublisher eventPub;
	
	public TreeViewListener(TreeViewHelper treeViewHelper, EventPublisher eventPub) {
		this.treeViewHelper = treeViewHelper;
		this.eventPub = eventPub;
		eventPub.register(this);
	}
	
	/* during testing I found out that neither PageCreateEvent nor PageMoveEvent guarantee that
	 * the page created or moved is already in its order once the event is triggered. This means
	 * that calling handleAddAndMove directly might not update the tree watches. Therefore I decided
	 * to put the page ID created or moved in a cache and check on each view event (which follows all
	 * create and move events) whether we have to update our tree watches for that page (at the view
	 * event the page order is fine).
	 */
	
	
	@EventListener
    public void onPageCreateEvent(PageCreateEvent event)
    {
		treeViewHelper.addPageEventToCache(event.getPage(), null);
    }

	
	@EventListener
    public void onPageMoveEvent(PageMoveEvent event)
    {
		// we only trigger our work for the parent page which is moved
		if(!event.isMovedBecauseOfParent()) {
			treeViewHelper.addPageEventToCache(event.getPage(), event.getOldParentPage());
		}
    }
	
	
	@EventListener
    public void onPageViewEvent(PageViewEvent event)
    {
		treeViewHelper.handleSinglePageEvent(event.getPage().getId(), false);
    }

	
	@Override
	public void destroy() throws Exception {
		eventPub.unregister(this);
	}
}
