package de.edrup.confluence.plugins;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.jmx.JmxSMTPMailServer;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.mail.server.MailServerManager;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;


// the TreeViewHelper class offers all functionality for handling tree-viewing
public class TreeViewHelper {
	
	private final BandanaManager bandanaMan;
	private final PageManager pageMan;
	private final NotificationManager notificationMan;
	private final UserAccessor userAcc;
	private final MultiQueueTaskManager mqtm;
	private final SettingsManager settingsMan;
	private final PermissionManager permissionMan;
	private final I18nResolver i18n;
	private final ClusterLockService clusterLock;
	private final TransactionTemplate transactionTemplate;
	private final XhtmlContent xhtmlUtils;
	private final MailServerManager mailServerMan;

	
	private final Cache<Long, TreeViewPageEvent> pageEventCache;
	private final Cache<String, String> bodyCache;
	
	private TreeViewConfiguration configuration;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewHelper.class);
	private static final Long minTimeDiff = 3000L;

	
	// constructor
	public TreeViewHelper(BandanaManager bandanaMan, PageManager pageMan,
			NotificationManager notificationMan, UserAccessor userAcc,
			MultiQueueTaskManager mqtm, SettingsManager settingsMan,
			PermissionManager permissionMan, I18nResolver i18n,
			CacheManager cacheMan, ClusterLockService clusterLock, 
			TransactionTemplate transactionTemplate, XhtmlContent xhtmlUtils,
			MailServerManager mailServerMan) {
		this.bandanaMan = bandanaMan;
		this.pageMan = pageMan;
		this.notificationMan = notificationMan;
		this.userAcc = userAcc;
		this.mqtm = mqtm;
		this.settingsMan  = settingsMan;
		this.permissionMan = permissionMan;
		this.i18n = i18n;
		this.clusterLock = clusterLock;
		this.transactionTemplate = transactionTemplate;
		this.xhtmlUtils = xhtmlUtils;
		this.mailServerMan = mailServerMan;
		
		pageEventCache = cacheMan.getCache(TreeViewHelper.class.getName() + ".cache", 
			null, new CacheSettingsBuilder().remote().expireAfterAccess(30, TimeUnit.MINUTES).build());
		bodyCache = cacheMan.getCache("Tree View email body cache", 
			null, new CacheSettingsBuilder().remote().expireAfterAccess(30, TimeUnit.SECONDS).build());
	}
	
	
	// add a watch to the list
	// a tree-view/watch is kept using the BandanaManager in the form userKey:contentId;userKey:contentId;...
	public void addWatch(ConfluenceUser u, Page page) {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewBandana.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
		    	// generate the new watch string to add
				String watch = u.getKey().toString() + ":" + page.getIdAsString() + ";";
				
				// get existing watches from bandana
				String existingWatches = getWatchesAsString();
				
				// check if we already have the watch - if not add it
				if(!existingWatches.contains(watch)) {
					existingWatches = existingWatches.concat(watch);
				}
				
				// write the new list of watches back
				bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.active-watches", existingWatches);
				
				log.info("A tree-watch was added for {} starting page {}.", u.getFullName(), page.getTitle());   
			}
			else {
			    log.error("Failed to lock TreeViewBandana for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewBandana due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}
	}
	
	
	// remove a watch from the list including all potential sub-watches
	// remark: I do not check whether the watch really exists; I simply call replace as this does not change anything
	// in case if does exist
	public void removeWatch(ConfluenceUser u, Page page) {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewBandana.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
		    	// generate the watch string to be removed
				String watch = u.getKey().toString() + ":" + page.getIdAsString() + ";";
				
				// get existing watches from bandana
				String existingWatches = getWatchesAsString();
				
				// remove watch
				existingWatches = existingWatches.replace(watch, "");

				// get all descendants
				List<Page> desc = pageMan.getDescendants(page);

				// loop through all descendants and also remove the watch of sub-watches (if they exist)
				for(Page dp : desc) {
					watch = u.getKey().toString() + ":" + dp.getIdAsString() + ";";
					existingWatches = existingWatches.replace(watch, "");
				}

				// write the new list of watches back
				bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.active-watches", existingWatches);
				
				log.info("The tree watch for {} starting page {} has been removed.", u.getFullName(), page.getTitle());	
			}
			else {
			    log.error("Failed to lock TreeViewBandan for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewBandana due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}
	}
	
	
	// get all watches
	public String getWatchesAsString() {
		
		// get value from bandana
		String existingWatches = (String) bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.active-watches");
		
		// in case the value does not exist return an empty string rather than null
		if(existingWatches == null) {
			existingWatches = "";
			log.debug("deEdrupTreeViewWatches was null in bandana.");
		}
		
		return existingWatches;
	}
	
	
	// set or un-set the watch flag in a transaction
	public void setWatchFlagInTransaction(ConfluenceUser user, Page startPage, boolean set) {
		transactionTemplate.execute(() -> setWatchFlag(user, startPage, set));
	}
	
	
	// set or un-set the watch flag for the startPage and all its descendants
	private boolean setWatchFlag(ConfluenceUser user, Page startPage, boolean set) {
				
    	// set/un-set the watch flag for the startPage
		if(set == true) {
			notificationMan.addContentNotification(user, startPage);
		}
		else {
			notificationMan.removeContentNotification(user, startPage);
		}
		
		// get all descendants
		List<Page> desc = pageMan.getDescendants(startPage);
		log.debug("Setting/removing the watch flag on {} descendants", desc.size());

		// loop through all descendants and set/un-set the watch
		for(Page dp : desc) {
			try {
				log.debug("Setting/removing watch flag on page: {}", dp.getId());
				if(set == true) {
					notificationMan.addContentNotification(user, dp);
				}
				else {
					notificationMan.removeContentNotification(user, dp);
				}
			}
			catch(Exception e) {
				log.error("Error setting/removing the watch flag on page: {}", dp.getId());
				log.error(e.toString());
			}
		}
		
		return true;
	}
	
	
	// check whether the page newBranch is affected by any of the watches
	// if case set the watch for the page and all its descendants
	// and send an email to the watcher
	private void handleAddAndMove(Page newBranch, Page oldParent) {
		
		if(newBranch == null) { return; }
		
		// debug stuff
		Date startTime = new Date();
		log.debug("handleAddAndMove called for: {}", newBranch.getTitle());
		
		// get the user currently logged in
		ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
		
		// generate a list of ancesteros of the new branch
		ArrayList<Page> nbAncestors = new ArrayList<Page>();
		nbAncestors.add(newBranch);
		nbAncestors.addAll(newBranch.getAncestors());
		ArrayList<Long> nbAncestorIDs = new ArrayList<Long>();
		for(Page nbAncestor : nbAncestors) {
			nbAncestorIDs.add(nbAncestor.getId());
		}
		
		// generate a list of ancestors of the old branch
		ArrayList<Page> opAncestors = new ArrayList<Page>();
		if(oldParent != null) {
			opAncestors.add(oldParent);
			opAncestors.addAll(oldParent.getAncestors());
		}
		ArrayList<Long> opAncestorIDs = new ArrayList<Long>();
		for(Page opAncestor : opAncestors) {
			opAncestorIDs.add(opAncestor.getId());
		}
		
		// get an array of watches
		ArrayList<String> watches = getWatchesAsArray();
		
		// for each watch in the list
		for(String watch : watches) {
			
			// extract the page ID from the watch and get the corresponding page object
			Long watchID = getPageIDFromWatch(watch);
			Page watchPage = pageMan.getPage(watchID);
						
			// check whether the old parent page (applies for a move only) was already part of the watch under evaluation
			boolean oldParentPartOfWatch = opAncestorIDs.contains(watchID);
			
			// special case related to bug #5: we are moving the watch itself
			if(watchID == newBranch.getId()) {
				oldParentPartOfWatch = true;
			}
			
			// is the new branch part of the descendants of the watch?
			boolean newBranchPartOfWatch = nbAncestorIDs.contains(watchID);
						
			//log.debug("Watch {} affected (part of watch, old parent in watch): {}, {}", watch, newBranchPartOfWatch, oldParentPartOfWatch);
			
			// extract the user key from the watch and get the user object
			String watchUserString = getUserKeyFromWatch(watch);
			ConfluenceUser watchUser = userAcc.getExistingUserByKey(new UserKey(watchUserString));
			
			// has the new parent an active watch?
			boolean newParentActiveWatch = notificationMan.isWatchingContent(watchUser, newBranch.getParent());
			
			/// we have to to something in case the new branch is part of the watch and the last parent was not part of the watch
			if(newBranchPartOfWatch && !oldParentPartOfWatch) {
												
				// in case it is a valid user
				if((watchUser != null) && (watchPage != null)) {
					// set the watch for that branch depending on the settings
					if(!getConfiguration().getConditionalWatch() || newParentActiveWatch) {
						setWatchFlagInTransaction(watchUser, newBranch, true);
					}
					
					// add some info to our log
					log.info("The watches for a branch where automatically added for {} starting page {}.", watchUser.getFullName(), newBranch.getTitle());
					
					// inform watchUser if indicated
					// we only send a notification by mail in case the user of the watch is different from the current user
					// and the watch user has view permission on the page where the added branch starts
					if(!watchUser.equals(currentUser) && permissionMan.hasPermission(watchUser, Permission.VIEW, newBranch)) {
						sendAddMoveInNotification(watchUser, watchPage, newBranch);
					}
				}
			}
			
			// when a branch is moved out of the watch
			// issue #8: only add a new watch in case the user can view that part
			if(oldParentPartOfWatch && !newBranchPartOfWatch && permissionMan.hasPermission(watchUser, Permission.VIEW, newBranch)) {
				if(watchUser != null) {
					// check if there is another tree watch of the user which covers to the moved branch
					if(isUserTreeWatchingPage(newBranch, watchUser) == 0L) {
						// if not add a new tree watch on the moved part and send a notification
						// remark: we keep the watch flags unchanged here
						addWatch(watchUser, newBranch);
						sendMoveOutNotification(watchUser, newBranch);
					}
				}
			}
		}
		
		// debug stuff
		Date endTime = new Date();
		log.debug("handleAddAndMove took {} ms", endTime.getTime() - startTime.getTime());
	}
	
	
	// check all watches whether user and page are still valid
	// if not remove the watch
	public void checkAllWatches() {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewBandana.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
				log.debug("checking all watches");
				
				// get all active tree watches
				String allWatches = getWatchesAsString();
				
				// split the string into a list of watches
				ArrayList<String> watches = getWatchesAsArray();
				
				// for each watch in the list
				for(String watch : watches) {
					
					// extract the page ID from the watch and check whether the page is still existing
					boolean pageValid = true;
					if(pageMan.getPage(getPageIDFromWatch(watch)) == null) {
						pageValid = false;
						log.debug("Invalid page found: {}", getPageIDFromWatch(watch));
					}
					
					// extract the user key from the watch and check whether the user is still existing
					String confUserString = getUserKeyFromWatch(watch);
					ConfluenceUser confUser = userAcc.getExistingUserByKey(new UserKey(confUserString));
					boolean userValid = true;
					if(confUser == null) {
						userValid = false;
						log.debug("Invalid user found: {}", confUserString);
					}
					// remark: unfortunately getExistingUserByKey does not always return null when the user is not existing any more
					// therefore I implemented this additional branch
					else {
						if(!userAcc.exists(confUser.getName())) {
							userValid = false;
							log.debug("Invalid user found: {}", confUserString);
						}
					}
					
					// issue #8: remove watches where the user has no view permission on
					if(pageValid && userValid) {
						if(!permissionMan.hasPermission(confUser, Permission.VIEW, pageMan.getPage(getPageIDFromWatch(watch)))) {
							pageValid = false;
						}
					}
					
					// remove watch from the allWatches string in case one piece is not valid any more
					if((pageValid == false) || (userValid == false)) {
						// remove watch
						// remark: the regex ? was added to remove potential wrong formatted watches
						String toReplace = watch.concat(";?");
						allWatches = allWatches.replaceAll(toReplace, "");
						log.info("The following watch was removed as it seems not to be valid any more: {}", watch);
					}
				}
				
				// write the new list of watches back
				bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.active-watches", allWatches);
			}
			else {
			    log.error("Failed to lock TreeViewBandana for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewBandana due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}

	}
	
	
	// return a link to the page with the link text = page title
	public String getTitleAsLink(Page p) {
		StringBuilder link = new StringBuilder();
		
		link.append("<a href=\"");
		link.append(settingsMan.getGlobalSettings().getBaseUrl());
		link.append("/pages/viewpage.action?pageId=");
		link.append(p.getIdAsString());
		link.append("\">");
		link.append(p.getTitle());
		link.append("</a>");
		
		return link.toString();
	}
	
	
	// send a notification when a page as been added or moved into a watch
	private void sendAddMoveInNotification(ConfluenceUser user, Page watchStart, Page branchStart) {
		
		// is there an email address set and is the user active? (bug #4)
		if((user.getEmail().length() > 0) && !userAcc.isDeactivated(user)) {
						
			// generate a new message
			ConfluenceMailQueueItem mqi = new ConfluenceMailQueueItem(user.getEmail(),
					getConfiguration().getAddedMailHeader().replace("$addedTitle", branchStart.getTitle()),
					getConfiguration().getAddedMailBody().replace("$addedTitle", branchStart.getTitle()).
					replace("$addedLink", getTitleAsLink(branchStart)).
					replace("$watchTitle", watchStart.getTitle()).
					replace("$watchLink", getTitleAsLink(watchStart)).
					replace("$userName", user.getFullName()).
					replace("$bodyHTML", convertStorageToMail(branchStart)),
					MIME_TYPE_HTML);
			if(mailServerMan.isDefaultSMTPMailServerDefined() && mailServerMan.getDefaultSMTPMailServer() instanceof JmxSMTPMailServer) {
				mqi.setFromName(((JmxSMTPMailServer) mailServerMan.getDefaultSMTPMailServer()).getFromName().replace("${fullname}", "Tree View"));
				log.debug(mqi.getFromName());
			}
			else {
				log.debug("No default SMTP server found!");
			}
			
			log.debug("Sending email to {} about {}.", user.getFullName(), branchStart.getTitle());
			
			// add the new message as task to the queue
			mqtm.addTask("mail", mqi);		
		}
	}
	
	
	// send a notification when a branch is moved out of a watch and is not covered by any other watch of the user
	private void sendMoveOutNotification(ConfluenceUser user, Page newBranch) {
		
		// is there an email address set and is the user active? (bug #4)
		if((user.getEmail().length() > 0) && !userAcc.isDeactivated(user)) {
			
			// generate a new message
			MailQueueItem mqi = new ConfluenceMailQueueItem(user.getEmail(),
					getConfiguration().getMovedOutMailHeader().replace("$addedTitle", newBranch.getTitle()),
					getConfiguration().getMovedOutMailBody().replace("$addedTitle", newBranch.getTitle()).
					replace("$addedLink", getTitleAsLink(newBranch)).
					replace("$userName", user.getFullName()),
					MIME_TYPE_HTML);
			
			log.debug("Sending email to {} about {}.", user.getFullName(), newBranch.getTitle());
			
			// add the new message as task to the queue
			mqtm.addTask("mail", mqi);		
		}
	}
	
	
	// convert the storage format to email
	private String convertStorageToMail(Page p) {
		try {
			String cacheKey = p.getIdAsString() + Integer.toString(p.getVersion());
			String cachedBody = bodyCache.get(cacheKey);
			if(cachedBody != null) {
				return cachedBody;
			}
			PageContext pc = p.toPageContext();
			pc.setOutputType(ConversionContextOutputType.EMAIL.value());
			DefaultConversionContext cc = new DefaultConversionContext(pc);
			String renderedBody = xhtmlUtils.convertStorageToView(p.getBodyAsString(), cc);
			bodyCache.put(cacheKey, renderedBody);
			return renderedBody;
		}
		catch(Exception e) {
			return e.toString();
		}
	}
	

	// get a list of all tree view starting pages for the given user
	public ArrayList<Page> getTreeViewsOf(ConfluenceUser user) {
		
		// the list of pages to return
		ArrayList<Page> treeViewsOf = new ArrayList<Page>();
		
		// get an array of all watches
		ArrayList<String> watches = getWatchesAsArray();
		
		// get key of given user
		String userKey = user.getKey().toString();
		
		// for each watch in the list
		for(String watch : watches) {
			
			// extract the page ID from the watch
			Page p = pageMan.getPage(getPageIDFromWatch(watch));
			
			// extract the user key from the watch
			String watchUserKey = getUserKeyFromWatch(watch);
			
			// add the page to the list in case the user matches
			if(userKey.equals(watchUserKey) && (p != null)) {
				treeViewsOf.add(p);
			}
		}
		
		// return the tree views of the given user
		return treeViewsOf;
	}
	
	
	// split the all watches string in bandana to an array of watches
	public ArrayList<String> getWatchesAsArray() {
		
		// get the all watches string from bandana
		String allWatches = getWatchesAsString();
		
		// create an empty array
		ArrayList<String> split = new ArrayList<String>();
		
		// perform split only in case allWatches is not empty
		// remark: String.split would return one element in case of an empty string
		if(allWatches.length() > 0) {
			split.addAll(Arrays.asList(allWatches.split(";")));
		}
		
		// return the watches as an array
		return split;
	}
	
	
	// get the page ID from a watch
	public Long getPageIDFromWatch(String watch) {
		try {
			String pageIDString = watch.substring(watch.indexOf(":") + 1);
			return Long.parseLong(pageIDString);
		}
		catch(NumberFormatException | IndexOutOfBoundsException e) {
			log.error("Malformed watch string found!");
			return 0L;
		}
	}
	
	
	// get user key from watch
	public String getUserKeyFromWatch(String watch) {
		try {
			return watch.substring(0, watch.indexOf(":"));
		}
		catch(IndexOutOfBoundsException e) {
			log.error("Malformed watch string found!");
			return "illigal";
		}
	}
	
	
	// check if user is watching page p by one of its tree watches
	public Long isUserTreeWatchingPage(Page p, ConfluenceUser user) {
		
		// get all watches of the user
		ArrayList<Page> usersTreeWatches = getTreeViewsOf(user);
		
		// get the ancestors of the page to check
		ArrayList<Page> ancestors = new ArrayList<Page>();
		ancestors.add(p);
		ancestors.addAll(p.getAncestors());
		
		// convert the list to list of page IDs
		ArrayList<Long> ancestorIDs = new ArrayList<Long>();
		for(Page ancestor : ancestors) {
			ancestorIDs.add(ancestor.getId());
		}
		
		// loop through all the watches
		for(Page usersTreeWatch : usersTreeWatches) {
			
			// if it contains the current page ID the user is watching the page
			if(ancestorIDs.contains(usersTreeWatch.getId())) {
				return usersTreeWatch.getId();
			}
		}
		
		// return that we found nothing
		return 0L;
	}
	
	
	// get the tree-view configuration data
	public TreeViewConfiguration getConfiguration() {
		
		// in case we don't have a valid configuration yet
		if(configuration == null) {
			
			// load it from Bandana
			configuration = (TreeViewConfiguration) bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.configuration.v1");
			
			// in case it was not found in Bandana
			if(configuration == null) {
				
				// initialize with the default values from i18n
				configuration = new TreeViewConfiguration();
				configuration.setAddedMailHeader(i18n.getText("de.edrup.confluence.plugins.tree-view.default.added.mail.header"));
				configuration.setAddedMailBody(i18n.getText("de.edrup.confluence.plugins.tree-view.default.added.mail.body"));
				configuration.setMovedOutMailHeader(i18n.getText("de.edrup.confluence.plugins.tree-view.default.movedout.mail.header"));
				configuration.setMovedOutMailBody(i18n.getText("de.edrup.confluence.plugins.tree-view.default.movedout.mail.body"));
				configuration.setConditionalWatch(false);
			}
		}
		
		// return the configuration
		return configuration;
	}
	
	
	// set the tree-view configuration data
	public void setConfiguration(TreeViewConfiguration configuration) {
		
		// set our local copy
		this.configuration = configuration;
		
		// and save it in Bandana
		bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.configuration.v1", configuration);
	}
	
	
	// add a page event to the cache
	public void addPageEventToCache(Page p, Page oldParent) {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewEventCache.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
		    	// in case the page ID is already in the cache remove it
				if(pageEventCache.get(p.getId()) != null) {
					pageEventCache.remove(p.getId());
					log.warn("Multipe cache events for the same page - only handling the last event");
				}
					
				// the new page event
				TreeViewPageEvent pe = null;
				if(oldParent == null) {
					pe = new TreeViewPageEvent(0L);
				}
				else
				{
					pe = new TreeViewPageEvent(oldParent.getId());
				}
				
				// put the new page vent into the cache
				pageEventCache.put(p.getId(), pe);
			}
			else {
			    log.error("Failed to lock TreeViewEventCache for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewEventCache due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}		
	}
	
	
	// handle the page event for page p in the cache
	// when checkTimer is set the event will only be handled in case a certain time has passed by
	// this is implemented to ensure that the page is already in its context
	public void handleSinglePageEvent(Long pageID, boolean checkTimer) {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewEventCache.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
		    	// check whether the cache contains a relevant entry
				if(pageEventCache.containsKey(pageID)) {
					
					// get the cache object
					TreeViewPageEvent pe = pageEventCache.get(pageID);
					
					// in case we have to check the time difference between now and the event
					if(checkTimer) {
						Date d = new Date();
						// in case the time difference is insufficient => return
						// by doing this we ant to ensure that the page is properly set into its hierarchy (e.g. the parent is right)
						if((d.getTime() - pe.getTimerTick()) < minTimeDiff) {
							return;
						}
					}
					
					// remove the entry from the cache
					pageEventCache.remove(pageID);
					
					// handle the event
					if(pe.getOldParentID() == 0L) {
						handleAddAndMove(pageMan.getPage(pageID), null);
					}
					else {
						handleAddAndMove(pageMan.getPage(pageID), pageMan.getPage(pe.getOldParentID()));
					}
				}
			}
			else {
			    log.error("Failed to lock TreeViewEventCache for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewEventCache due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}		
	}
	
	
	// handle all potential left overs in the page event cache 
	public void clearPageEventCache() {
		
		ClusterLock lock = clusterLock.getLockForName("TreeViewEventCache.taskExecutionLock");
		try {
			if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
		    	// get all page IDs in the cache
				ArrayList<Long> pageIDs = new ArrayList<Long>();
				pageIDs.addAll(pageEventCache.getKeys());
				
				// handle them all keeping an eye on the time difference (checkTimer = true)
				for(Long pageID:pageIDs) {
					handleSinglePageEvent(pageID, true);
				}
			}
			else {
			    log.error("Failed to lock TreeViewEventCache for 100 ms");
			}
		}
		catch(InterruptedException e) {
			log.error("Failed to lock TreeViewEventCache due to an interrupted exception");
		}
		finally {
			lock.unlock();
		}
	}
}