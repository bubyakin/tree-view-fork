package de.edrup.confluence.plugins;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.actions.AbstractUserProfileAction;
import com.atlassian.confluence.pages.Page;
import java.util.ArrayList;

public class ManageTreeViews extends AbstractUserProfileAction {

	private static final long serialVersionUID = 42L;
	final TreeViewHelper treeViewHelper;

	public ManageTreeViews(TreeViewHelper treeViewHelper) {
		this.treeViewHelper = treeViewHelper;
	}
	
	@Override
	public String execute() throws Exception
	{
		return super.execute();
	}
	
	public ArrayList<Page> getTreeViews() {
		
		// get the user currently logged in
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// return the list of tree views for the current user
		return treeViewHelper.getTreeViewsOf(confluenceUser);
	}   
}
