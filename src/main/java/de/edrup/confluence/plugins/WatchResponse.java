package de.edrup.confluence.plugins;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "treeWatch")
@XmlAccessorType(XmlAccessType.FIELD)
public class WatchResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "id")
	private Long pageID;
	
	@XmlElement(name = "username")
	private String userName;
	
	public WatchResponse() {
	}
	
	public WatchResponse(Long pageID, String userName) {
		this.pageID = pageID;
		this.userName = userName;
	}
}
