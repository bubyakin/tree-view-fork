package de.edrup.confluence.plugins;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;

public class WatchTree extends ConfluenceActionSupport implements PageAware {

	static final long serialVersionUID = 42L;
	
	private AbstractPage page;
	final TreeViewHelper treeViewHelper;
	
	public WatchTree(TreeViewHelper treeViewHelper) {
        this.treeViewHelper = treeViewHelper;
	}

	@Override
	public AbstractPage getPage() {
		return page;
	}

	@Override
	public boolean isLatestVersionRequired() {
		return true;
	}

	@Override
	public boolean isPageRequired() {
		return true;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	@Override
	public void setPage(AbstractPage page) {
		this.page = page;
	}
	
	public String execute()
	{
		// get the user currently logged in
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// is it a named user?
		if(confluenceUser != null) {
			
			// set the watch flag for this page an all descendants
			treeViewHelper.setWatchFlagInTransaction(confluenceUser, (Page) getPage(), true);
			
			// add the tree view to our list
			treeViewHelper.addWatch(confluenceUser, (Page) getPage());
		}
		return "success";
	}

}
