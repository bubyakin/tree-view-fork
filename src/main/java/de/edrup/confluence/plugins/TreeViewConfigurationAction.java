package de.edrup.confluence.plugins;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.webwork.ServletActionContext;

public class TreeViewConfigurationAction extends ConfluenceActionSupport {

	private static final long serialVersionUID = 42L;
	
	private final TreeViewHelper treeViewHelper;
	
	public TreeViewConfigurationAction(TreeViewHelper treeViewHelper) {
        this.treeViewHelper = treeViewHelper;
	}
	
	@Override
	public String execute() throws Exception {
		
		super.execute();
        
		HttpServletRequest request = ServletActionContext.getRequest();
        TreeViewConfiguration configuration = treeViewHelper.getConfiguration();
        
        // in case the request contains our first input field we assume that we have a POST call here
        if(request.getParameter("addedHeader") != null) {
	        configuration.setAddedMailHeader(request.getParameter("addedHeader"));
	        configuration.setAddedMailBody(request.getParameter("addedBody"));
	        configuration.setMovedOutMailHeader(request.getParameter("movedOutHeader"));
	        configuration.setMovedOutMailBody(request.getParameter("movedOutBody"));
	        if(request.getParameter("conditionalWatch") != null) {
       		 configuration.setConditionalWatch(true);
	       	}
	       	else {
	       		 configuration.setConditionalWatch(false);
	       	}
	        
	        treeViewHelper.setConfiguration(configuration);
        }
        
        return SUCCESS;
    }
	
	public TreeViewConfiguration getConfiguration() {
		return treeViewHelper.getConfiguration();
	}
}
