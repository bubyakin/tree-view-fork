AJS.toInit(function () {
    if(AJS.Meta.get("content-type") == "page") {
        // TreeView REST endpoints
	    var isCoveredLink   = AJS.contextPath() + "/rest/treeviewrest/1.0/" + AJS.params.pageId + "/isCovered";
	    var watchTreeLink   = AJS.contextPath() + "/rest/treeviewrest/1.0/" + AJS.params.pageId + "/watchTree";
    	var unwatchTreeLink = AJS.contextPath() + "/rest/treeviewrest/1.0/" + AJS.params.pageId + "/unwatchTree";

        var treeViewDiv = $("<div class='checkbox'><input type='checkbox' class='checkbox' id='cw-watch-tree'>" +
                                    "<label for='cw-watch-tree'>" +
                                        AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.watch.tree.checkbox') +
                                    "</label>" +
                               "</div>");

        // Get status for current tree
        $.getJSON( isCoveredLink, function( data ) {
            var isCovered = data.result.startsWith("true");
            if(isCovered) {
                $(treeViewDiv).find("#cw-watch-tree").prop('checked', true);

                // Additional logic for descendant:
                var isRoot = data.result.includes(AJS.params.pageId);
                var isDescendant = !isRoot;
                if(isDescendant) {
                    //   1. Disable checkbox
                    $(treeViewDiv).find("#cw-watch-tree").prop('disabled', true);
                    //   2. Change text with link to root
                    var parentId = data.result.substr(5);
                    var rootLink = AJS.contextPath() + "/pages/viewpage.action?pageId=" + parentId
                    var text = AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.watch.tree.checkbox.descendant')
                                .replace("$link", rootLink);
                    $(treeViewDiv).find("label[for='cw-watch-tree']").html(text);
                }
            }
        });

		$('#watch-content-button').click(function () {
			setTimeout(function () {
			        // Add Tree checkbox as a third option into Watching menu
				    $('#inline-dialog-confluence-watch form').append(treeViewDiv);
                    // And add listeners on change event for checkboxes
                    initChangeListeners();
				}, 70);
		});

	}

    function initChangeListeners(watchPage, watchTree) {
            var watchPage = $("#cw-watch-page");
            var watchTree = $(treeViewDiv).find("#cw-watch-tree");
            // Put change listener on Tree checkbox
            $(watchTree).change(function () {
                if ($(this).is(':checked')) {
                    $.get( watchTreeLink )
                        .done(function() {
                            $(watchPage).prop('checked', true);
                        });
            	} else {
                    $.get( unwatchTreeLink )
					    .done(function() {
					         if( !$(watchPage).prop('disabled')) {
					            $(watchPage).prop('checked', false);
					         }
                        });
            	}
            });
    }

});